package inheritance;
import java.util.*;

public class Student extends Person{
	String nim;
	public Student() {
		System.out.println("Inside Student:Constructor");
		super.name="Anda";
	}
	
	public String getNim()
	{
		return nim;
	}
	
	@Override
	public void identity()
	{
		System.out.println("Jml tagihan " + name + " : " + hitungPembayaran());
		System.out.println("NIM: "+getNim());
		super.identity();
		/*System.out.println("Nama: "+super.name);
		System.out.println("Alamat: "+super.address);*/
	}
	
	public void job()
	{
		System.out.println("Pekerjaan : Mahasiswa");
	}
	
	public String getName() {
		System.out.println("Student name: "+name);
		return name;
	}

    public void input(){
        super.input();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Masukkan NIM : ");
        nim = scanner.nextLine();
    }

    public double hitungPembayaran(){
        double tagihan = 0;
        double spp = 0;
        double sks = 0;
        double modul = 0;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Masukkan spp : ");
        spp = scanner.nextDouble();
        System.out.println("Masukkan sks : ");
        sks = scanner.nextDouble();
        System.out.println("Masukkan modul : ");
        modul = scanner.nextDouble();
        
        return tagihan = spp + sks + modul;
    }
}
